#include "../src/Bottle.hpp"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE( "init Chimay" ) {
    const Bottle b {"Chimay", 0.75, 0.08};
    REQUIRE(b._name == "Chimay");
    REQUIRE(b._vol == 0.75);
    REQUIRE(b._deg == 0.08);
}

TEST_CASE( "init Orange Pur Jus" ) {
    const Bottle b {"Orange Pur Jus", 1, 0};
    REQUIRE(b._name == "Orange Pur Jus");
    REQUIRE(b._vol == 1);
    REQUIRE(b._deg == 0);
}

TEST_CASE( "alcoholVolume Chimay" ) {

    Bottle b {"Chimay",.75,.08};
    double resultat = alcoholVolume(b);
    REQUIRE (resultat == 0.06);
}

TEST_CASE( "alcoholVolume Orange Pur Jus" ) {

    // TODO
    Bottle b {"Orange Pur Jus",1,0};
    double resultat = alcoholVolume(b);
    REQUIRE (resultat == 0);
}

TEST_CASE( "alcoholVolume Chimay + Orange" ) {
    Bottle b {"Chimay",.75,.08};
    Bottle bs {"Orange Pur Jus",1,0};
     double resultat = alcoholVolume(b)+alcoholVolume(bs);
      REQUIRE (resultat == 0.06);


}

TEST_CASE( "alcoholVolume Chimay + Chimay" ) {
 Bottle b {"Chimay",.75,.08};
 Bottle bs {"Chimay",.75,.08};
    double resultat = alcoholVolume(b)+alcoholVolume(bs);
    REQUIRE (resultat == 0.12);
}

TEST_CASE( "isPoivrot Chimay + Orange" ) {
    // TODO
     Bottle b {"Chimay",.75,.08};
    Bottle bs {"Orange Pur Jus",1,0};
     double resultat = isPoivrot(b)+isPoivrot(bs);
      REQUIRE (resultat == 0.06);

}

TEST_CASE( "isPoivrot Chimay + Chimay" ) {

    Bottle b {"Chimay",.75,.08};
 Bottle bs {"Chimay",.75,.08};
    double resultat = isPoivrot(b)&& isPoivrot(bs);
    REQUIRE (resultat == 0.12);
}

TEST_CASE( "readBottles Chimay" ) {
    Bottle b {"Chimay",.75,.08};
    double resultat = readBottles(b);
    REQUIRE (resultat == 0.06);
}

TEST_CASE( "readBottles Chimay + Orange" ) {
   Bottle b {"Chimay",.75,.08};
    Bottle bs {"Orange Pur Jus",1,0};
     double resultat = readBottles(b)+readBottles(bs);
      REQUIRE (resultat == 0.06);
}
    