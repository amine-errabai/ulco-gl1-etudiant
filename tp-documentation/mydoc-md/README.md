#this is mydoc -md

#license
this project is under the MIT

#List
*item1
*item2
##TABLE
column1 | column2
------- | -------
 foo    |    bar
 #code
 ```hs
 main::IO()
 main = putstrln("Hello")
 ```
 #Quote
 I never said half the crap people said I did .
 Albert Einstein
 #image
 ![Image](http://httpsfr.wikipedia.org/wiki/Visual_Studio_Code#/media/Fichier:Visual_Studio_Code_1.35_icon.svg)
